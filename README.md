# Surfaces

A mod that provides an interface for other mods to create new surfaces on different "layers" of "planets". It provides transportation of entities, items and fluids between these surfaces.

Heavily inspired by the [Surfaces mod by Simcra](https://mods.factorio.com/mod/Surfaces), but it has other goals, so I decided to write my own mod from scratch.

In order to play with this mod, addon mods must be used.

- Define new surfaces with custom generation
- Surface registration: register ownership of certain layers of certain planets
- Transport: various types of "portals" enabling transport of players, items, fluids and energy.

# Getting Started

To get started immediately, download one of the [Configuration Mods](#configuration-mods). 

For more a more customized experience, you can [create your own Configuration mod](#creating-your-own-configuration-mod).

For modders, see [Documentation](#api-documentation).

# Addon mods

## Surface generation

These mods generate new surfaces. They can expose an interface allowing the main library to call them when they are needed.

[Surfaces Caves](https://mods.factorio.com/mod/Surfaces_Caves) ([source](https://gitlab.com/Danacus/surfaces-caves))  
[Surfaces Original](https://mods.factorio.com/mod/Surfaces_Original) ([source](https://gitlab.com/Danacus/surfaces-original))  

## Transport

These mods add new entities for transportation. They can register portals such that all the transportation of players, items, etc. will be handled by the library.

[Surfaces Transport](https://mods.factorio.com/mod/Surfaces_Transport) ([source](https://gitlab.com/Danacus/surfaces-transport))  

## Configuration mods

In order to actually play the game, you will want to use a configuration mod. These mods will pull all dependencies and decide which layer of which planet will be managed by which addon mod. For example: you could have caves right beneath the main surface provided by one mod, an open sky provided by another mod right above and perhaps even space provided by yet another mod. 

[Surfaces Cave Configuration](https://mods.factorio.com/mod/Surfaces_Config_Caves) ([source](https://gitlab.com/Danacus/surfaces-cave-configuration))  
[Surfaces Default Configuration](https://mods.factorio.com/mod/Surfaces_Config_Default) ([source](https://gitlab.com/Danacus/surfaces-default-configuration))  

# Modding

## Creating your own configuration mod

The easiest way to way to get started is by cloning or downloading the [Configuration Template](https://gitlab.com/Danacus/surfaces-configuration-template).
Make sure to extract the archive if you downloaded it as a zip file.

1. Rename the downloaded folder to `<your mod name>_1.0.0` where `your mod name` is the name of your mod.
2. Place this folder in the `mods` folder of the game. (see [Factorio Wiki](https://wiki.factorio.com/Application_directory) for more information)
3. Edit `info.json`. `name` must match with the name you chose earlier. You can also fill in title, author and description fields. In the dependencies field you must enter all mods Surfaces addons you would like to include.
In the template, Surfaces Original and Surfaces Transport were added, feel free to replace them. You can find the name of a mod by checking the URL on the Mod portal page.
4. Now we can move on to `control.lua`. Read the text inside the file and make the modification you want.
5. If everything went well, you can now run the game and enable you mod.

## API Documentation

The Surfaces API is exposed through interfaces (more information: [LuaRemote](https://lua-api.factorio.com/latest/LuaRemote.html)).

### register_surface(name, main_surface, layers, owner)

Registers a surface generator for a given main surface on given layers. Usually called by configuration mods, not by the surface generation mods.

`name`: A unique name shared between the layers of the given main surface. This name will be sent to the generation mods when the surface needs to be created or generated.  
`main_surface`: The main surface is the "0-layer" surface. For example: "nauvis"  
`layers`: Select which layers should be controlled by the given surface owner. Must either be a number or a table `{<from>, <to>}` where
`<from>` and `<to>` are either a number, "bottom" or "top". Negative numbers are below the main surface and positive numbers are above the main surface.  
`owner`: The owner of the surface is an interface provided by a surface generation addon mod. This interface will control all the specified layers of the given main surface.  

### get_main_surfaces()

Returns a table with all main surfaces ("0-layer" surfaces like nauvis) configured by the user in the mod settings.

### get_main_surface(surface)

Get the name of the main surface to which the given surface belongs. For example, `get_main_surface("nauvis-caves:3")` might return the string "nauvis".

`surface`: A `LuaSurface` object or name of an existing surface.

### get_surface_layer(surface)

Get the index of a surface (`LuaSurface`) as a layer of a main surface. For example, `get_surface_layer("nauvis-caves:3")` might return `3`.

`surface`: A `LuaSurface` object or name of an existing surface.

### get_surface(main_surface, index)

Get the layer of the given main surface with at the given index. For example, `get_surface("nauvis", -4)` might return the `LuaSurface` with name "nauvis-caves:-4" if the caves mod is configured to be used at layer `-4`.
Note: the surface will only be returned if it has been created. For example, if the player has not yet visited layer 42, `get_surface("nauvis", 42)` will return `nil`.
If you wish to create the surface in case it does not exist yet, use `get_or_create_surface` instead.

`main_surface`: A `LuaSurface` object or name of an existing surface that is registered as a main surface (e.g. nauvis).  
`index`: Integer that represents the "layer" of a planet. Negative integers represent layers below the main surface, while positive integers represent layers above the main surface.

### get_or_create_surface(main_surface, index)

Same as `get_surface`, but creates the surface if it does not exist yet.

### add_main_surface(name)

Adds the given surface to the table of surfaces returned by `get_main_surfaces`.

### add_replaceable_tiles(tiles)

Specify which tiles can be replace by other tiles when placing portals. For example: allow walls to be replaced with floor.

`tiles`: A table with key-value pairs such that the key tile can be replaced by the value tile.

### add_replaceable_entities(entities)

Specify which entities can be replace by other entities when placing portals.

`tiles`: A table with key-value pairs such that the key entity can be replaced by the value entity.

### register_portal(portal_spec)

Register a new portal.

`portal_spec`: A valid [Portal Specification](#portal-specification).

### add_pair(portal_spec, from_entity, to_entity)

Register a pair of already built entities as a portal pair. Useful when you want to handle placement of portal
entities by your mod, but use the automatic transport functionality from Surfaces.

`portal_spec`: A portal specification, only `type` must be provided. Optionally, `type_params` and `allow_reverse` can be provided.

### Portal Specification

A portal specification describes which entity prototypes are used for a certain portal. Many examples can be found [here](https://gitlab.com/Danacus/surfaces-transport/-/blob/master/control.lua).

- `type`: The type of portal: entity, fluid, etc. (look [here](https://gitlab.com/Danacus/factorio-surfaces/-/tree/master/script/transport) for the up-to-date list)  
- `type_params`: Extra parameters for certain transport types, usually optional.  
- `relative_target`: Defines how many layers we should shift, for example `-1` for the layer right beneath the surface the `from_entity` was placed on.  
- `absolute_target`: Overrides relative_target of defined, the layer number of the target layer.  
- `from`: Table describing the source portal
    - `prototype`: Name of the prototype
    - `restrictions`: Table with restrictions (optional)
        - `main_surface`: Name of the main surface on which this entity may be placed, leave empty for any
        - `layer`: On which layer can this be placed? Empty for any (not implemented yet!)
- `to`: Table describing the target portal
    - `prototype`: Name of the prototype
    - `main_surface`: The main surface on which the target portal will be placed. Useful for transportation across different main surfaces (optional).
    - `clear_radius`: Radius around the portal that must be cleared, defaults to the size of the entity (optional).
- `allow_reverse`: Boolean, if true the portal can be used in reverse. For example: the player portals from Surfaces Transport can be used in reverse, but the chests only work in one direction.
It should only be used for entity transportation, as "balancing" features for other portal types are not implemented.
